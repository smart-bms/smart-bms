# Smart BMS by Reactor
This group is intended for Smart BMS software, description and other resources.

# What is Smart BMS?
Smart BMS is an attempt to create next generation, fully managed, simple and scalable battery management system for battery powered devices like
electric vehicles, robots and other.
The solution was notivated by lack of such scalable device in the market. Most of the currently used battery management systems are usually 
passive systems with no availability of customization, dedicated for some devices, hard to customize or have limited capabilities.
Smart BMS by design is a very scalable device with general purpose communication ports, custom termistors, wide range of cell count
supported and availability to be easy adapted to any battery projects.
It is based on common STM32 microcontroler and powered by Zephyr OS which makes it easy to use device's capabilities and implement custom algorithms.
Alongside device's firmware, project consists of open communication protocol, open source desktop application to configure the device and a mobile app
to monitor battery's parameters and allow final user to invoke some actions.

# Device's firmware
Firmware is released on GPL license and based on Zephyr OS. It implements communication with all peripherals on the board, custom serial protocol
to communicate with BMS from external device, protocols to communicate with some well known devices, like Xiaomi M365. By design firmware
should be scalable and easy to extend by adding other devices communication or algorithms. This makes Smart BMS be perfect solution for building
custom battery replacements as well as use it as a base for completely new products.

# Smart BMS board
The board is designed by **Reactor** and fits in as small size as it was possible. It has general puspose communication ports, I/O pins,
status indication LED, and easy to access programming port (SWD). All these components are available to the user via software.

# Desktop application
The main purpose of desktop application is to monitor and configure parameters of the device, assuming it uses default (or extended) firmware.
I talks to the device via specified custom protocol based on simple UART transmission. It allows the user to set up correct limits of the
voltage, current, temperatures or read some data from the device, like event history.
The application itself is based on very common Gtk4 toolkit which makes it's easy to run on any of the most common desktop operating systems,
including most Linux distributions, MS Windows and Mac OS.

# Mobile application
The mobile application aims to run on Android and iOS based devices. Comparing to base desktop application, it provides more simple and clear
user interface to show device's parameters, but lacks some configuration options that require more advanded communication or cannot be done
remotely.

# Extending device
As the Smart BMS implements general purpose communication ports, I/O pins and I2C, it can be easily extended by other peripheral devices like
sensors, Bluetooth adapters. Or the BMS itself can act as a peripheral to deliver informations about battery to the master device. Such
hardware design is commonly used in electric vehicles like Mija M365, where engine controler is the main device's computer that process
input data and takes an action, while BMS is an external device that delivers information about battery, including all cells voltages,
battery health, current that flow in or out the battery, or the battery status itself. In this case, Smart BMS could be perfect replacement
for proprietary original BMS, as it supports full communication with the main controler but supports much more, including higher limit of
maximum current.

# Energy consumption
One of the most important design rule is to make Smart BMS use as little energy as possible while still be able to analyze data and events in
real time and support 115200 UART baudrate. The last thing is the reason why device uses external 3.6864 crystal oscillator so MCU core's clock
can be set up to just 921.6MHz to save the energy, yet still be able to communicate at 115200 bits/s baudrate.

# Heat dispersion
BMS's printed circuit board is designed in the way to distract heat from balancing or high current load. Still, the default board has the
ability to sense temperature from three sources, where one of them is embedded in the device itself to monitor it's temperature. Two others
can be stick via a wire anywhere, for example between the battery cells.

